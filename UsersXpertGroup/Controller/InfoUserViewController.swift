//
//  InfoUserViewController.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import UIKit
import JGProgressHUD

class InfoUserViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var id: Int = 1
    var name: String = "Anonymous"
    var email: String = "Anonymous"
    var phone: String = "Anonymous"

    let hud = JGProgressHUD()
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var collectionPosts: UICollectionView!
    
    var posts = [Post]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//      Delegados collection
        collectionPosts.dataSource = self
        collectionPosts.delegate = self
        collectionPosts.register(UINib.init(nibName: "PostViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        getPosts(urlUsers: "posts")
//      Poner datos traidos desde el view controller pasado
        lblName.text = name
        lblEmail.text = email
        lblPhone.text = phone
        
    }
    
//    MARK: - CONSUMIR SERVICIO
    func getPosts(urlUsers: String){
        showLoadingHUD()
        let urlService = URL(string: Text.urlRoot + urlUsers)
            
            URLSession.shared.dataTask(with: urlService!){ (data, response, error) in
                if error == nil {
                    do {
                        
                        self.posts = try JSONDecoder().decode([Post].self, from: data!)
                        //self.realVideos = self.videos
                        
                        
                    } catch let error{
                        print(error)
                    }
                    
                    DispatchQueue.main.async {
                        self.collectionPosts.reloadData()
                    }
                }
                
            }.resume()
        self.hud.dismiss(animated: true)
        }

//    MARK: - DELEGADOS COLLECTION
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PostViewCell
         
        cell.lblTitle.text = posts[indexPath.row].title.capitalized
        cell.lblBody.text = posts[indexPath.row].body.capitalized
             
        
        return cell
    }
    
//  MARK: - DIALOGO DE CARGA
    func showLoadingHUD() {
        
        hud.vibrancyEnabled = true
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        
        hud.detailTextLabel.text = "Cargando"
        hud.show(in: self.view)
            
    }

}

//
//  SearchBarView.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import UIKit

class SearchBarView: UICollectionReusableView {
        
    @IBOutlet weak var searchBar: UISearchBar!
    
}

//
//  ViewController.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import UIKit
import JGProgressHUD

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var CollectionUsers: UICollectionView!
    var usersDecodable = [UserDecodable]()
    var users = [User]()
    var realUsers = [User]()
    let hud = JGProgressHUD()
    var db: UsersDBHelper = UsersDBHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//      Delegados collection
        CollectionUsers.dataSource = self
        CollectionUsers.delegate = self
        CollectionUsers.register(UINib.init(nibName: "UserViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
//      Si hay datos en la base de datos carguelos, sino, consuma el servicio
        if db.read().isEmpty{
            getUsers(urlUsers: "users")
        } else {
            users = db.read()
            realUsers = users
            showToast(message: "Cargando desde sqlite...", font: .systemFont(ofSize: 12.0))
        }
        
        
    }
    
//    MARK: - CONSUMIR SERVICIO
    func getUsers(urlUsers: String){
        showLoadingHUD()
        let urlService = URL(string: Text.urlRoot + urlUsers)
            
        URLSession.shared.dataTask(with: urlService!){ [self] (data, response, error) in
                if error == nil {
                    do {
                        
//                      Traer la data
                        self.usersDecodable = try JSONDecoder().decode([UserDecodable].self, from: data!)
                        
//                      Pasar la data al modelo User para trabajar con este
                        for item in self.usersDecodable {
                            
                            self.users.append(User(id: item.id, name: item.name, email: item.email, phone: item.phone))
                        }
                        self.realUsers = self.users
                        
//                      Insertar datos
                        insertUsersDb()
                        
                    } catch let error{
                        print(error)
                    }
                    
                    DispatchQueue.main.async {
                        self.CollectionUsers.reloadData()
                    }
                }
                
            }.resume()
        
        self.hud.dismiss(animated: true)
        }
    
    
//    MARK: - INSERTAR DATOS
    func insertUsersDb(){
        print(realUsers.count)
        for item in users {
            db.insert(id: item.id, name: item.name, email: item.email, phone: item.phone)
        }
    }

    
//    MARK: - DELEGADOS COLLECTION
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UserViewCell
         
        cell.lblName.text = users[indexPath.row].name.capitalized
        cell.lblMail.text = users[indexPath.row].email.capitalized
        cell.lblPhone.text = users[indexPath.row].phone.capitalized
             
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "InfoUser", bundle: nil)
        let infoUser = storyboard.instantiateViewController(withIdentifier: "infouser") as! InfoUserViewController
        infoUser.id = users[indexPath.row].id
        infoUser.name = users[indexPath.row].name
        infoUser.email = users[indexPath.row].email
        infoUser.phone = users[indexPath.row].phone
        infoUser.modalPresentationStyle = .formSheet
        self.present(infoUser, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let searchView: UICollectionReusableView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "searchBar", for: indexPath)
        return searchView
    }
    
//    MARK: - SEARCH BAR
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if(!(searchBar.text?.isEmpty)!){
            //reload your data source if necessary
            self.CollectionUsers?.reloadData()
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(!searchText.isEmpty){
            //reload your data source if necessary
            self.users.removeAll()
            for item in self.realUsers{
                if item.name.lowercased().contains(searchBar.text!.lowercased()) {
                    self.users.append(item)
                }
            }
            
            
        } else {
            self.users = self.realUsers
            
        }
        self.CollectionUsers.reloadData()
        if users.isEmpty {
            self.showToast(message: "List is empty", font: .systemFont(ofSize: 12.0))
        }
    }

//  MARK: - DIALOGO DE CARGA
    func showLoadingHUD() {
        
        hud.vibrancyEnabled = true
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        
        hud.detailTextLabel.text = "Cargando"
        hud.show(in: self.view)
            
    }

}

// MARK: - MOSTRAR TOAST
extension UIViewController {

func showToast(message : String, font: UIFont) {

    let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
    toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    toastLabel.textColor = UIColor.white
    toastLabel.font = font
    toastLabel.textAlignment = .center;
    toastLabel.text = message
    toastLabel.alpha = 1.0
    toastLabel.layer.cornerRadius = 10;
    toastLabel.clipsToBounds  =  true
    self.view.addSubview(toastLabel)
    UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
         toastLabel.alpha = 0.0
    }, completion: {(isCompleted) in
        toastLabel.removeFromSuperview()
    })
} }

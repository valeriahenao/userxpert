//
//  Post.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import Foundation

struct Post : Decodable{
    var id : Int
    var title : String
    var body : String
}

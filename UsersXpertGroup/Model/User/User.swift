//
//  User.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import Foundation

struct UserDecodable : Decodable{
    var id : Int
    var name : String
    var phone : String
    var email : String
}

class User{
    
    var name: String = ""
    var email: String = ""
    var phone: String = ""
    var id: Int = 0
    
    init(id:Int, name:String, email:String, phone:String)
    {
        self.id = id
        self.name = name
        self.email = email
        self.phone = phone
    }
    
}

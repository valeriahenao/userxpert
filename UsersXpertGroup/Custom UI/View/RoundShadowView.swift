//
//  RoundShadowView.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import UIKit

@IBDesignable class RoundShadowView: UIView {

    @IBInspectable var radius : CGFloat = 0{
        didSet{
            self.applyMask()
        }
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.applyMask()
    }
    
    func applyMask()
    {
//      Corner radius
        clipsToBounds = true
        layer.masksToBounds = false
        layer.cornerRadius = radius
        
//      Sombra
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 3
    }

}

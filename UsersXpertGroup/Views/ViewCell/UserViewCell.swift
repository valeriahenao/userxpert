//
//  UserViewCell.swift
//  UsersXpertGroup
//
//  Created by Andres Henao on 11/20/21.
//

import UIKit

class UserViewCell: UICollectionViewCell {

    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
